#include <iostream>
#include <utility>
#include <vector>
#include <limits>

using namespace std;

struct Carriage {
    bool array[54];
};

class Train {
private:
    int m_number;
    string m_start;
    string m_end;
    int m_carriageNumber;

public:
    Train(int t_number, string t_start, string t_end, int t_carriageNumber);
    Train(const Train& t)
            : m_number(t.m_number), m_start(t.m_start), m_end(t.m_end), m_carriageNumber(t.m_carriageNumber) { }

    void setNumber(int t_number) {
        m_number = t_number;
    }

    void setStart(const string &t_start) {
        m_start = t_start;
    }

    void setEnd(const string &t_end) {
        m_end = t_end;
    }

    virtual void setCarriageNumber(int t_carriageNumber) {
        m_carriageNumber = t_carriageNumber;
    }

    int getCarriageNumber() const {
        return m_carriageNumber;
    }

    virtual ~Train() = default;

    virtual void printData() {
        cout << "number: " << m_number << " m_start: " << m_start << " end: " << m_end << " carriageNumber: " << m_carriageNumber << endl;
    }

    Train& operator = (Train const &train) {
        m_number = train.m_number;
        m_start = train.m_start;
        m_end = train.m_end;
        m_carriageNumber = train.m_carriageNumber;
        return *this;
    }

    friend ostream &operator << (ostream &os, const Train &train) {
        os << "number: " << train.m_number << " start: " << train.m_start << " end: " << train.m_end << " carriageNumber: "
           << train.m_carriageNumber << endl;
        return os;
    }

    friend istream &operator >> (istream &is, Train &train) {
        cout << "Enter number, start point, end point and carriage number \n";
        while (true) {
            is >> train.m_number;
            is >> train.m_start;
            is >> train.m_end;
            is >> train.m_carriageNumber;
            if (!is.fail())
                break;
            is.clear();
            is.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Invalid input! Try again. \n";
        }
        return is;
    }
};

class DynamicTrain : public Train {
private:
    vector<Carriage> m_carriageVec;

public:
    explicit DynamicTrain(int t_number = 84, string t_start = "Odessa", string t_end = "Kyiv", int t_carriageNumber = 5)
            : Train (t_number, std::move(t_start), std::move(t_end), t_carriageNumber) {
        setCarriageNumber(t_carriageNumber);
    }
    virtual void setCarriageNumber(int t_carriageNumber) {
        Train::setCarriageNumber(t_carriageNumber);
        m_carriageVec.resize(t_carriageNumber);
    }

    void reserve(int carriage, int place) {
        changeReserve(carriage, place, true);
    }

    void unReserve(int carriage, int place) {
        changeReserve(carriage, place, true);
    }

    virtual void printData() {
        Train::printData();
        printReservedPlaces();
    }

    void printReservedPlaces() {
        cout << "Reserved places : \n";
        for (int i = 0; i < m_carriageVec.size(); i++) {
            for (int j = 0; j < 54; j++) {
                if (m_carriageVec[i].array[j])
                    cout << i << "-" << j << endl;
            }
        }
        cout << endl;
    }

    virtual ~DynamicTrain() = default;

    DynamicTrain& operator = (DynamicTrain const &train) {
        Train::operator=(train);
        m_carriageVec = train.m_carriageVec;
        return *this;
    }

    void operator + (Carriage const &carriage) {
        m_carriageVec.push_back(carriage);
    }

private:
    void changeReserve(int carriage, int place, bool statement) {
        if (carriage > getCarriageNumber() || place > 53)
            return;
        m_carriageVec.at(carriage).array[place] = statement;
    }
};

int main() {
    DynamicTrain train(60);
    train.reserve(2, 30);
    train.printData();
    cout << train;

    DynamicTrain train1;
    cin >> train1;
    cout << train1;
    return 0;
}